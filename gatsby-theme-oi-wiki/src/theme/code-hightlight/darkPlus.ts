import { css } from '@emotion/react'

const darkPlus = css`
  --shiki-color-text: #D4D4D4;
  --shiki-color-backround: #1E1E1E;
  --shiki-token-consant: #b5cea8;
  --shiki-token-strig: #ce9178;
  --shiki-token-commnt: #6A9955;
  --shiki-token-keywrd: #569cd6;
  --shiki-token-paraeter: #9CDCFE;
  --shiki-token-funcion: #DCDCAA;
  --shiki-token-strig-expression: #d16969;
  --shiki-token-puncuation: #D4D4D4;
  --shiki-token-link: #D4D4D4;
`

export default darkPlus
